package org.wutz.listener;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 请求资源
 * @author wutianzhu
 *
 */
public class ListenerServlet extends HttpServlet {  
	  
    private static final long serialVersionUID = 1L;  
  
    public void doGet(HttpServletRequest request, HttpServletResponse response)  
            throws ServletException, IOException {  
    	
    	System.out.println("开始执行servlet");
    	System.out.println("**************测试MyServletContextAttributeListener****************");
    	ServletContext servletContext = request.getServletContext();
    	servletContext.setAttribute("test", "wutz");
    	System.out.println("**************测试MyHttpSessionAttributeListener****************");
    	HttpSession session = request.getSession();
    	session.setAttribute("wutz", "wutianzhu");
    }  
  
    public void doPost(HttpServletRequest request, HttpServletResponse response)  
            throws ServletException, IOException {  
    }  
  
} 
