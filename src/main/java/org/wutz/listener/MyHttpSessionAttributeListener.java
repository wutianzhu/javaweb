package org.wutz.listener;

import java.text.MessageFormat;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

/**
 * HttpSessionAttributeListener监听器的使用
 * @author wutianzhu
 *
 */
public class MyHttpSessionAttributeListener implements HttpSessionAttributeListener{

	@Override
	 public void attributeAdded(HttpSessionBindingEvent se) {
	  String str =MessageFormat.format(
	    "HttpSession域对象中添加了属性:{0}，属性值是:{1}"
	    ,se.getName()
	    ,se.getValue());
	  System.out.println(str);
	 }
	 
	 @Override
	 public void attributeRemoved(HttpSessionBindingEvent se) {
	  String str =MessageFormat.format(
	    "HttpSession域对象中删除属性:{0}，属性值是:{1}"
	    ,se.getName()
	    ,se.getValue());
	  System.out.println(str);
	 }
	 
	 @Override
	 public void attributeReplaced(HttpSessionBindingEvent se) {
	  String str =MessageFormat.format(
	    "HttpSession域对象中替换了属性:{0}的值"
	    ,se.getName());
	  System.out.println(str);
	 }

}
