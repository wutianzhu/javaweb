package org.wutz.listener;


import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * HttpSessionListener
 * @author wutianzhu
 *
 */
public class MyHttpSessionListener implements HttpSessionListener{

	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		System.out.println("MyHttpSessionListener.sessionCreated()");
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent arg0) {
		System.out.println("MyHttpSessionListener.sessionDestroyed()");
	}


}
