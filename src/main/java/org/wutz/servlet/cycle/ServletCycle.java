package org.wutz.servlet.cycle;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;

/**
 * servlet生命周期详解
 * @author wutianzhu
 */
public class ServletCycle extends HttpServlet {
	
	/**
	 * 执行时机：当Servlet实例创建的时候被调用，做初始化工作，只会被调用一次
	 * ServletConfig：Servlet的配置对象，初始化的时候可以配置
	 * 
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);   //防止在service中调用getServletContext()报空指针异常
		
		//获取ServletContext对象
		ServletContext servletContext = config.getServletContext();
		
		//获取工程目录webroot下文件的绝对路径
		//getRealPath参数内容不会被校验，只有真正要用这个路径的时候才知道路径对不对
		String path = servletContext.getRealPath("test.properties");
		System.out.println(path);
		
		//获得classpath下资源的文件流，因为classpath下的文件发布之后是在/WEB-INF/classes下
		//所以去指定/WEB-INF/classes/test.properties
		InputStream inputStream = servletContext.getResourceAsStream("/WEB-INF/classes/test.properties");
		
		//使用类加载器的方式来读取classpath下的资源文件，好处是不依赖于ServletContext，任何类都可以获取classpath下的资源文件，
		//不需要指定/WEB-INF/classes
		//InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/WEB-INF/classes/test.properties");
		//以上方式是不对的
		InputStream inputStream1 = this.getClass().getClassLoader().getResourceAsStream("test.properties");
		
		Properties prop = new Properties();
		try {
			prop.load(inputStream);
			String value = prop.getProperty("key");
			System.out.println(value);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 执行时机：当一个请求来请求当前servlet的时候被调用
	 * 处理当前servlet的业务逻辑并且把相关内容返回给浏览器
	 */
	@Override
	public void service(ServletRequest arg0, ServletResponse arg1)
			throws ServletException, IOException {
		// 获取ServletContext对象
		ServletContext servletContext = getServletContext();
		// /获取全局容器参数
		Integer count = (Integer) servletContext.getAttribute("pvcount");
		if (count == null) {
			//count = 1;
			servletContext.setAttribute("pvcount", 1);
		} else {
			servletContext.setAttribute("pvcount", ++count);
		}
		count = (Integer) servletContext.getAttribute("pvcount");
		String result = "<font color='red' size='10'>当前被点击了"+count+" 次 </font>";

		arg1.getOutputStream().write(result.getBytes());
	}

	/**
	 * 执行时机：Servlet对象销毁的时候调用
	 * 做一些收尾和清理工作
	 */
	@Override
	public void destroy() {
		System.out.println("ServletCycle.destroy()");
	}
	
	/**
	 * 获取ServletConfig 的配置对象
	 */
	@Override
	public ServletConfig getServletConfig() {
		return super.getServletConfig();
	}

	/**
	 * 获取当前Servlet的一些属性信息
	 */
	@Override
	public String getServletInfo() {
		return null;
	}
}
