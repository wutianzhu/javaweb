package org.wutz.servlet.config;

import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

/**
 * 获取servlet初始化参数
 * @author wutianzhu
 */
public class ServletInitParam extends HttpServlet {
	
	/**
	 * 执行时机：当Servlet实例创建的时候被调用，做初始化工作，只会被调用一次
	 * ServletConfig：Servlet的配置对象，初始化的时候可以配置
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		//获取单个参数
		String username = config.getInitParameter("name");
		System.out.println(username);
		
		//获取多个参数
		Enumeration enums = config.getInitParameterNames();
		while(enums.hasMoreElements()){
			String name = (String) enums.nextElement();
			String value = config.getInitParameter(name);
			System.out.println(value);
		}
	}

}
