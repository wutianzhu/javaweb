package org.wutz.servlet.config;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;

/**
 * 服务器启动时创建Servlet
 * @author wutianzhu
 */
public class LoadOnServerTwo extends HttpServlet {
	
	/**
	 * 执行时机：当Servlet实例创建的时候被调用，做初始化工作，只会被调用一次
	 * ServletConfig：Servlet的配置对象，初始化的时候可以配置
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);  
		System.out.println("LoadOnServerTwo.init()");
	}

	/**
	 * 执行时机：当一个请求来请求当前servlet的时候被调用
	 * 处理当前servlet的业务逻辑并且把相关内容返回给浏览器
	 */
	@Override
	public void service(ServletRequest arg0, ServletResponse arg1)
			throws ServletException, IOException {
	}
}
