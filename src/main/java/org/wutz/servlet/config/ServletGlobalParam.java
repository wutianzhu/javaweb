package org.wutz.servlet.config;


import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

/**
 * 获取全局初始化参数
 * @author wutianzhu
 */
public class ServletGlobalParam extends HttpServlet {
	
	/**
	 * 执行时机：当Servlet实例创建的时候被调用，做初始化工作，只会被调用一次
	 * ServletConfig：Servlet的配置对象，初始化的时候可以配置
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		//获取单个参数
		ServletContext servletContext = config.getServletContext();
		String username = servletContext.getInitParameter("num");
		System.out.println(username);
		
	}

}
