package org.wutz.servlet.helloworld;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;

/**
 * 第一个Servlet实例
 * @author wutianzhu
 *
 */
public class HelloWorld extends HttpServlet {
	

	@Override
	public void service(ServletRequest arg0, ServletResponse arg1)
			throws ServletException, IOException {
		System.out.println("第一个servlet例子");
	}
}
